package compiler.abstree;

import java.io.*;

/** Opis WHILE zanke. */
public class AbsWhileStmt extends AbsStmt {

	/** Pogoj. */
	public AbsValExpr cond;

	/** Jedro zanke. */
	public AbsStmts stmts;

	public AbsWhileStmt(AbsValExpr cond, AbsStmts stmts) {
		this.cond = cond;
		this.stmts = stmts;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"WHILE\">\n");
		descsToXML(xml);
		cond.toXML(xml);
		stmts.toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

}
