package compiler.abstree;

import java.io.*;

/** Opis kazalcev. */
public class AbsPtrTyp extends AbsTypExpr {

	/** Tip podatka, na katereka kaze kazalec. */
	public AbsTypExpr base;

	public AbsPtrTyp(AbsTypExpr base) {
		this.base = base;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"PTR\">\n");
		descsToXML(xml);
		base.toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

}
