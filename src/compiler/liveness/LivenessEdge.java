package compiler.liveness;

import compiler.frames.Temp;

public class LivenessEdge
{
	private Temp t0;
	private Temp t1;
	
	public LivenessEdge(Temp t0, Temp t1)
	{
		this.t0 = t0;
		this.t1 = t1;
	}
	
	public Temp getT0()
	{
		return t0;
	}
	
	public Temp getT1()
	{
		return t1;
	}
	
	@Override
	public String toString()
	{
		return t0.name() + "<->" + t1.name();
	}

	@Override
	public int hashCode()
	{
		return t0.hashCode() + t1.hashCode();
	}

	@Override
	public boolean equals(Object obj)
	{
		return this.hashCode() == obj.hashCode();
	}
}
