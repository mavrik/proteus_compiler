package compiler.asmcode;

import java.util.Iterator;
import java.util.LinkedList;

import compiler.frames.Frame;
import compiler.imcode.ImcChunk;
import compiler.imcode.ImcCodeChunk;
import compiler.imcode.ImcStmt;

public class Main {

	/** Izvede prevajanje do faze izracuna linearne vmesne kode.  */
	public static void exec() {
		/* Prevajanje do faze izracuna linearizirane vmesne kode.  */
		compiler.lincode.Main.exec();
		System.out.println("Generating assembly code...");
		/* Testni izpis generirane kode za vse funkcije.  */
		Iterator<ImcChunk> chunks = compiler.imcode.Main.chunks.iterator();
		while (chunks.hasNext()) {
			ImcChunk chunk = chunks.next();
			if (chunk instanceof ImcCodeChunk) {
				ImcCodeChunk codeChunk = (ImcCodeChunk)chunk;
				LinkedList<AsmInstr> asmCode = generateAsmCode(codeChunk.lincode, codeChunk.frame);
				System.out.print("\nFUNCTION " + codeChunk.frame.fun.name + ":\n");
				Iterator<AsmInstr> instrs = asmCode.iterator();
				while (instrs.hasNext()) {
					AsmInstr instr = instrs.next();
					System.out.println(instr.format(null));
				}
			}
		}
	}

	//---------------------------------------------------------------------------------------------------------------------

	public static LinkedList<AsmInstr> generateAsmCode(ImcStmt stmt, Frame callFrame) 
	{
		AsmCodeCreator creator = new AsmCodeCreator(callFrame);
		creator.munch(stmt);
		
		return creator.instructions;
	}

}
