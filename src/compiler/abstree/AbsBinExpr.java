package compiler.abstree;

import java.io.*;

/** Opis binarnega izraza.  */
public class AbsBinExpr extends AbsValExpr {

	public static final int ADD = 0;
	public static final int SUB = 1;
	public static final int MUL = 2;
	public static final int DIV = 3;
	public static final int EQU = 4;
	public static final int NEQ = 5;
	public static final int LTH = 6;
	public static final int GTH = 7;
	public static final int LEQ = 8;
	public static final int GEQ = 9;
	public static final int AND = 10;
	public static final int OR  = 11;
	public static final int ARR = 12;
	public static final int DOT = 13;
	public static final int IS  = 14;

	/** Vrsta binarne operacije.  */
	public int op;

	/** Levi podizraz.  */
	public AbsValExpr lsub;

	/** Desni podizraz.  */
	public AbsValExpr rsub;

	public AbsBinExpr(int op, AbsValExpr lsub, AbsValExpr rsub) {
		this.op = op;
		this.lsub = lsub;
		this.rsub = rsub;
	}

	@Override
	public void toXML(PrintStream xml) {
		String op = null;
		switch (this.op) {
		case ADD: op = "+" ; break;
		case SUB: op = "-" ; break;
		case MUL: op = "*" ; break;
		case DIV: op = "/" ; break;
		case EQU: op = "=="; break;
		case NEQ: op = "!="; break;
		case LTH: op = "&#60;" ; break;
		case GTH: op = "&#62;" ; break;
		case LEQ: op = "&#60;="; break;
		case GEQ: op = "&#62;="; break;
		case AND: op = "&#38;" ; break;
		case OR : op = "|" ; break;
		case ARR: op = "[]"; break;
		case DOT: op = "." ; break;
		case IS : op = "=" ; break;
		}
		xml.print("<absnode kind=\"BIN\" value=\"" + op + "\">\n");
		descsToXML(xml);
		lsub.toXML(xml);
		rsub.toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

}
