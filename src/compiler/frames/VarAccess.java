package compiler.frames;

import java.io.*;

import compiler.abstree.*;

/** Dostop do globalne spremenljivke.  */
public class VarAccess extends Access {

	/** Opis spremenljivke.  */
	public AbsVarDecl var;

	/** Labela spremenljivke.  */
	public Label label;

	public VarAccess(AbsVarDecl var) {
		this.var = var;
		label = Label.newLabel(var.name);
	}

	public void toXML(PrintStream xml) {
		xml.print("<frmnode>\n<frm kind=\"label\" value=\"" + label.name() + "\"/>\n</frmnode>\n");
	}

}
