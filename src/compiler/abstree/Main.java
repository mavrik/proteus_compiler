package compiler.abstree;

import java.io.*;

import compiler.report.*;
import compiler.lexanal.*;
import compiler.synanal.*;

public class Main {

	/** Abstraktno sintaksno drevo prevajanega programa.  */
	public static AbsTree absTree = null;

	/**
	 * Izvede prevajanje do faze gradnje abstraktnega sintaksnega drevesa.
	 */
	public static void exec() {
		System.out.println("Building abstract tree...");
		/* Odpremo vhodno in izhodno datoteko.  */
		FileReader srcFile = null;
		String srcName = compiler.Main.prgName + ".proteus";
		try { srcFile = new FileReader(srcName); }
		catch (FileNotFoundException _) { Report.error("Source file '" + srcName + "' cannot be opened.", 1); }
		PrintStream xml = XML.open("abstree");

		ProteusLex lexer = new ProteusLex(srcFile);
		ProteusSyn parser = new ProteusSyn(lexer);
		try {
			absTree = (AbsTree)(parser.parse().value);
			absTree.toXML(xml);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			Report.error("Error while testing abstract syntax tree construction.", 1);
		}

		/* Zapremo obe datoteki.  */
        XML.close("abstree", xml);
        try { srcFile.close(); }
		catch (IOException _) { Report.error("Source file '" + srcName + "' cannot be opened.", 1); }
	}
}
