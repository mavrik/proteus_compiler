package compiler.abstree;

import java.io.PrintStream;

/**
 * Deklaracija: tip, funkcija ali spremenljivka.
 *
 * @see AbsTypDecl
 * @see AbsFunDecl
 * @see AbsVarDecl
 */
public abstract class AbsDecl extends AbsTree {

	/** Deklarirano ime. */
	public String name;

	/** Zgradba deklariranega tipa ali tip funkcije ali spremenljivke.  */
	public AbsTypExpr typ;

	public AbsDecl(String name, AbsTypExpr typ) {
		this.name = name;
		this.typ = typ;
	}

	@Override
	public void descsToXML(PrintStream xml) {
		if (declId != -1) {
			xml.print("<semnode kind=\"DECL.ID:" + declId + "\"/>");
		}
		super.descsToXML(xml);
	}

	// SEMANTICNA ANALIZA:

	public int declId = 0;

}
