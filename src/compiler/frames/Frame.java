package compiler.frames;

import java.io.*;
import java.util.*;

import compiler.report.*;
import compiler.abstree.*;

/** Klicni zapis funkcije.  */
public class Frame implements XMLable {

	/** Opis funkcije.  */
	public AbsFunDecl fun;

	/** Staticni nivo funkcije.  */
	public int level;

	/** Vstopna labela.  */
	public Label label;

	/** Stevilo argumentov.  */
	public int numArgs;

	/** Lokalne spremenljivke funkcije.  */
	LinkedList<LocAccess> locVars;

	/** Velikost bloka lokalnih spremenljivk.  */
	public int sizeLocs;

	/** Velikost bloka za oldFP in retAddr.  */
	public int sizeFPRA = 8;

	/** Velikost bloka zacasnih spremenljivk.  */
	public int sizeTmps;

	/** Velikost bloka registrov.  */
	public int sizeRegs;

	/** Velikost izhodnih argumentov.  */
	public int sizeArgs;

	/** Kazalec FP.  */
	public Temp FP;

	/** Spremenljivka z rezultatom funkcije.  */
	public Temp RV;

	public Frame(AbsFunDecl fun, int level) {
		this.fun = fun;
		this.level = level;
		this.label = (level == 0 ? Label.newLabel(fun.name) : Label.newLabel());
		this.numArgs = 0;
		this.locVars = new LinkedList<LocAccess> ();
		this.sizeLocs = 0;
		this.sizeFPRA = 8;
		this.sizeTmps = 0;
		this.sizeRegs = 40;
		this.sizeArgs = 0;
		FP = new Temp();
		RV = new Temp();
	}

	/** Velikost klicnega zapisa.  */
	public int size() {
		return sizeLocs + sizeFPRA + sizeTmps + sizeRegs + sizeArgs;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<frmnode>\n");
		xml.print("<frm kind=\"level\" value=\"" + level + "\"/>\n");
		xml.print("<frm kind=\"label\" value=\"" + label.name() + "\"/>\n");
		xml.print("<frm kind=\"size\" value=\"" + size() + "\"/>\n");
		xml.print("<frm kind=\"FP\" value=\"" + FP.name() + "\"/>\n");
		xml.print("<frm kind=\"RV\" value=\"" + RV.name() + "\"/>\n");
		xml.print("</frmnode>\n");
	}

}
