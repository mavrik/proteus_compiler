package compiler.abstree;

public interface Visitor {

	public Object visit(AbsArrTyp node);
	public Object visit(AbsAtomExpr node);
	public Object visit(AbsAtomTyp node);
	public Object visit(AbsBinExpr node);
	public Object visit(AbsDecls node);
	public Object visit(AbsForStmt node);
	public Object visit(AbsFunDecl node);
	public Object visit(AbsFunName node);
	public Object visit(AbsIfStmt node);
	public Object visit(AbsLetExpr node);
	public Object visit(AbsPtrTyp node);
	public Object visit(AbsRecTyp node);
	public Object visit(AbsStmts node);
	public Object visit(AbsTypDecl node);
	public Object visit(AbsTypName node);
	public Object visit(AbsUnExpr node);
	public Object visit(AbsValStmt node);
	public Object visit(AbsVarDecl node);
	public Object visit(AbsVarName node);
	public Object visit(AbsWhileStmt node);

}
