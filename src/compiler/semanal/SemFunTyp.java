package compiler.semanal;

import java.io.*;
import java.util.*;

import compiler.report.*;

public class SemFunTyp extends SemTyp {

	public Vector<SemTyp> args;

	public SemTyp result;

	public SemFunTyp(SemTyp result) {
		args = new Vector<SemTyp>();
		this.result = result;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<semnode kind=\"FUN\">\n");
		Iterator<SemTyp> argTyps = this.args.iterator();
		while (argTyps.hasNext())
			argTyps.next().toXML(xml);
		result.toXML(xml);
		xml.print("</semnode>\n");
	}

	@Override
	public int size() {
		Report.error("Internal error.", 1);
		return -1;
	}
}
