package compiler.abstree;

/**
 * Opis vrednosti.
 *
 * @see AbsAtomExpr
 * @see AbsVarName
 * @see AbsFunName
 * @see AbsBinExpr
 * @see AbsUnExpr
 */
public abstract class AbsValExpr extends AbsTree {

}
