package compiler.frames;

/** Opis zacasne spremenljivke v programu.  */
public class Temp {

	private static int count = 0;

	private int num;

	public Temp() {
		num = count++;
	}

	@Override
	public boolean equals(Object t) {
		return num == ((Temp)t).num;
	}

	public String name() {
		return "T" + num;
	}

	@Override
	public int hashCode()
	{
		return num;
	}
}
