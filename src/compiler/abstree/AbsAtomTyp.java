package compiler.abstree;

import java.io.*;

/** Opis atomarnih tipov. */
public class AbsAtomTyp extends AbsTypExpr {

	public static final int BOOL   = 0;
	public static final int CHAR   = 1;
	public static final int INT    = 2;
	public static final int STRING = 3;
	public static final int VOID   = 4;

	/** Tip. */
	public int typ_desc;

	public AbsAtomTyp(int typ_desc) {
		this.typ_desc = typ_desc;
	}

	@Override
	public void toXML(PrintStream xml) {
		String typ_name = null;
		switch (typ_desc) {
		case BOOL  : typ_name = "BOOL"  ; break;
		case CHAR  : typ_name = "CHAR"  ; break;
		case INT   : typ_name = "INT"   ; break;
		case STRING: typ_name = "STRING"; break;
		case VOID  : typ_name = "VOID"  ; break;
		}
		xml.print("<absnode kind=\"" + typ_name + "\">\n");
		descsToXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

}
