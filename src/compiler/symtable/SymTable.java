package compiler.symtable;

import java.util.*;

import compiler.abstree.*;

/** Simbolna tabela. */
public class SymTable
{

	/** Globalna simbolna tabela. */
	public static SymTable names = new SymTable();

	/**
	 * Interna simbolna tabela.
	 * 
	 * Za vsako ime hranimo seznam deklaracij. Trenutno veljavna deklaracija je
	 * v glavi seznama; ostale deklaracije so nevidne, ker pripadajo znunanjim
	 * nivojem deklaracij.
	 */
	private HashMap<String, LinkedList<AbsDecl>> table;

	/**
	 * Seznam nivojev deklaracij.
	 * 
	 * Vsak seznam nivoja deklaracij vsebuje vsa imena, ki so bila deklarirana
	 * na tem nivoju deklaracij. Trenutni nivo deklaracij je v glavi glavnega
	 * seznama.
	 */
	private LinkedList<LinkedList<String>> scopes;

	/** Ustvari zacetno simbolno tabelo. */
	private SymTable()
	{
		table = new HashMap<String, LinkedList<AbsDecl>>();
		scopes = new LinkedList<LinkedList<String>>();
		newScope();
	}

	/**
	 * Ustvari nov nivo deklaracij imen.
	 * 
	 * Novo ime ustvarimo tako, da pripravimo nov, prazen seznam imen, katerih
	 * deklaracije bodo prekrile deklaracije istih imen na prejsnjih nivojih
	 * deklaracij.
	 */
	public void newScope()
	{
		LinkedList<String> scope = new LinkedList<String>();
		scopes.addFirst(scope);
	}

	/**
	 * Razveljavi trenutni nivo deklaracij imen.
	 * 
	 * Vsem imenom, ki so deklarirana na trenutnem nivoju deklaracij, odstranimo
	 * trenutno veljavno deklaracijo. Ce je to zadnja deklaracija tega imena, to
	 * ime odstranimo iz simbolne tabele.
	 */
	public void oldScope()
	{
		LinkedList<String> scope = scopes.getFirst();
		for (String identifier : scope)
		{
			LinkedList<AbsDecl> decls = table.get(identifier);
			decls.removeFirst();
			if (decls.isEmpty())
				table.remove(identifier);
			else
				table.put(identifier, decls);
		}
		scopes.removeFirst();
	}

	/**
	 * Vstavi novo deklaracjo imena v simbolno tabelo in s tem prekrije
	 * morebitno prejsnjo deklaracijo.
	 * 
	 * @param identifier
	 *            Ime.
	 * @param decl
	 *            Deklaracija.
	 * @throws SymDefinedAtThisScope
	 *             Ce ima to ime deklaracijo na tem nivoju deklaracij.
	 */
	public void put(String identifier, AbsDecl decl)
			throws SymDefinedAtThisScope
	{
		if (scopes.getFirst().contains(identifier))
			throw new SymDefinedAtThisScope(identifier);
		LinkedList<AbsDecl> decls = table.get(identifier);
		if (decls == null)
			decls = new LinkedList<AbsDecl>();
		decls.addFirst(decl);
		table.put(identifier, decls);
		LinkedList<String> scope = scopes.getFirst();
		scope.addFirst(identifier);
		scopes.removeFirst();
		scopes.addFirst(scope);
	}

	/**
	 * Vrne trenutno veljavno deklaracijo imena.
	 * 
	 * @param identifier
	 *            Ime.
	 * @return Trenutno veljavna deklaracija imena.
	 * @throws SymUndefinedAtThisScope
	 *             Ce ime ni definirano na tem nivoju deklaracij.
	 */
	public AbsDecl get(String identifier) throws SymUndefinedAtThisScope
	{
		LinkedList<AbsDecl> decls = table.get(identifier);
		if (decls == null)
			throw new SymUndefinedAtThisScope(identifier);
		else
			return decls.getFirst();
	}

}
