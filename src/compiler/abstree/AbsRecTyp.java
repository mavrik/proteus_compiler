package compiler.abstree;

import java.io.*;
import java.util.*;

/** Opis zapisov. */
public class AbsRecTyp extends AbsTypExpr {

	/** Opis posameznih komponent. */
	public LinkedList<AbsVarDecl> comps;

	public AbsRecTyp(LinkedList<AbsVarDecl> comps) {
		this.comps = comps;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"REC\">\n");
		descsToXML(xml);
		Iterator<AbsVarDecl> comps = this.comps.iterator();
		while (comps.hasNext())
			comps.next().toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

}
