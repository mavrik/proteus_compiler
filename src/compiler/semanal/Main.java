package compiler.semanal;

import java.io.*;

import compiler.report.*;
import compiler.abstree.*;
import compiler.frames.Frame;
import compiler.symtable.*;

public class Main {

	/** Izvede prevajanje do faze semanticne analize. */
	public static void exec() {
		/* Zgradimo abstraktno sintaksno drevo.  */
		compiler.abstree.Main.exec();

		System.out.println("Doing semantic analysis...");
		// Vstavimo standardne funkcije v simbolno tabelo.
		{
			int id = 0;
			// fun new(p:ptr):void;
			{
				AbsFunDecl funDecl = new AbsFunDecl("new", null, null, null);
				funDecl.frame = new Frame(funDecl, 0);
				
				id--; funDecl.declId = id;
				funDecl.semTyp = new SemFunTyp(new SemAtomTyp(SemAtomTyp.VOID));
				((SemFunTyp)funDecl.semTyp).args.add(new SemPtrTyp(null));
				try { SymTable.names.put(funDecl.name, funDecl); }
				catch (SymDefinedAtThisScope _) {}
			}
			// fun free(p:ptr):void;
			{
				AbsFunDecl funDecl = new AbsFunDecl("free", null, null, null);
				funDecl.frame = new Frame(funDecl, 0);
				id--; funDecl.declId = id;
				funDecl.semTyp = new SemFunTyp(new SemAtomTyp(SemAtomTyp.VOID));
				((SemFunTyp)funDecl.semTyp).args.add(new SemPtrTyp(null));
				try { SymTable.names.put(funDecl.name, funDecl); }
				catch (SymDefinedAtThisScope _) {}
			}
			// fun read_char():char;
			{
				AbsFunDecl funDecl = new AbsFunDecl("read_char", null, null, null);
				funDecl.frame = new Frame(funDecl, 0);
				id--; funDecl.declId = id;
				funDecl.semTyp = new SemFunTyp(new SemAtomTyp(SemAtomTyp.CHAR));
				try { SymTable.names.put(funDecl.name, funDecl); }
				catch (SymDefinedAtThisScope _) {}
			}
			// fun read_int():int;
			{
				AbsFunDecl funDecl = new AbsFunDecl("read_int", null, null, null);
				funDecl.frame = new Frame(funDecl, 0);
				id--; funDecl.declId = id;
				funDecl.semTyp = new SemFunTyp(new SemAtomTyp(SemAtomTyp.INT));
				try { SymTable.names.put(funDecl.name, funDecl); }
				catch (SymDefinedAtThisScope _) {}
			}
			// fun print_char(c:char):void;
			{
				AbsFunDecl funDecl = new AbsFunDecl("print_char", null, null, null);
				funDecl.frame = new Frame(funDecl, 0);
				id--; funDecl.declId = id;
				funDecl.semTyp = new SemFunTyp(new SemAtomTyp(SemAtomTyp.VOID));
				((SemFunTyp)funDecl.semTyp).args.add(new SemAtomTyp(SemAtomTyp.CHAR));
				try { SymTable.names.put(funDecl.name, funDecl); }
				catch (SymDefinedAtThisScope _) {}
			}
			// fun print_int(i:int):void;
			{
				AbsFunDecl funDecl = new AbsFunDecl("print_int", null, null, null);
				funDecl.frame = new Frame(funDecl, 0);
				id--; funDecl.declId = id;
				funDecl.semTyp = new SemFunTyp(new SemAtomTyp(SemAtomTyp.VOID));
				((SemFunTyp)funDecl.semTyp).args.add(new SemAtomTyp(SemAtomTyp.INT));
				try { SymTable.names.put(funDecl.name, funDecl); }
				catch (SymDefinedAtThisScope _) {}
			}
		}

		/* Opravimo oba preleta semanticne analize.  */
		try	{
			compiler.abstree.Main.absTree.accept(new NameResolver());
			compiler.abstree.Main.absTree.accept(new TypeResolver());
		}
		catch (StackOverflowError _) {
			Report.error("Cyclic type definition.", 1);
		}

		/* Izpisemo rezultat. */
		PrintStream xml = XML.open("semanal");
		compiler.abstree.Main.absTree.toXML(xml);
		XML.close("semanal", xml);
	}

}
