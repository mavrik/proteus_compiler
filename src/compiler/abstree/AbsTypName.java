package compiler.abstree;

import java.io.*;

/** Opis poimenovanega tipa. */
public class AbsTypName extends AbsTypExpr {

	/** Ime tipa. */
	public String name;

	public AbsTypName(String name) {
		this.name = name;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"TYP\" value=\"" + name + "\">\n");
		descsToXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public void descsToXML(PrintStream xml) {
		if (decl != null) xml.print("<semnode kind=\"DECL.ID:" + decl.declId + "\"/>");
		super.descsToXML(xml);
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

	// SEMANTICNA ANALIZA

	public AbsTypDecl decl = null;

}
