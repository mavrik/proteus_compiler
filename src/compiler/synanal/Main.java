package compiler.synanal;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;

import compiler.lexanal.ProteusLex;
import compiler.report.Report;
import compiler.report.XML;

public class Main {

	public static String[] proteusNontNames;

	static {
		/* Pripravimo imena vrst vmesnih simbolov.  */
		ProteusTok proteusTok = new ProteusTok();
		Field[] proteusToks = proteusTok.getClass().getDeclaredFields();
		proteusNontNames = new String[proteusToks.length];
		for (int f = 0; f < proteusToks.length; f++) {
			try {
				int tok = proteusToks[f].getInt(proteusTok);
				String lex = proteusToks[f].toString().replaceAll("^.*\\.", "");
				if (! ((tok < compiler.lexanal.Main.proteusTermNames.length) &&
					   (lex.equals(compiler.lexanal.Main.proteusTermNames[tok])))) {
					proteusNontNames[tok] = lex;
				}
			}
			catch (IllegalAccessException _) {}
		}
	}

	/** Izvede prevajanje do vkljucno faze sintaksne analize. */
	public static void exec() {
		System.out.println("Syntactic analysis...");
		/* Odpremo vhodno in izhodno datoteko.  */
		FileReader srcFile = null;
		String srcName = compiler.Main.prgName + ".proteus";
		try { srcFile = new FileReader(srcName); }
		catch (FileNotFoundException _) { Report.error("Source file '" + srcName + "' cannot be opened.", 1); }
		PrintStream xml = XML.open("synanal");

		ProteusLex lexer = new ProteusLex(srcFile);
		ProteusSyn parser = new ProteusSyn(lexer);
		try {
			parser.debug_parse(xml);
		}
		catch (Exception ex) {
			XML.close("synanal", xml);
			Report.error("Error while testing syntax analyzer.", 1);
		}

		/* Zapremo obe datoteki.  */
        XML.close("synanal", xml);
        try { srcFile.close(); }
		catch (IOException _) { Report.error("Source file '" + srcName + "' cannot be opened.", 1); }
	}
}
