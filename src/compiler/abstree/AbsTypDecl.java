package compiler.abstree;

import java.io.*;

/** Deklaracija tipa. */
public class AbsTypDecl extends AbsDecl {

	public AbsTypDecl(String name, AbsTypExpr typ) {
		super(name, typ);
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"TYP DECL\" value=\"" + name + "\">\n");
		descsToXML(xml);
		typ.toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

	@Override
	public void descsToXML(PrintStream xml) {
		super.descsToXML(xml);
		if (semTyp != null) xml.print("<frmnode>\n<frm kind=\"size\" value=\"" + semTyp.size() + "\"/></frmnode>\n");
	}

}
