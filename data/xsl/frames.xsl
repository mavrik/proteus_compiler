<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="frames">
  <html>
    <body>
    <table cellspacing="0">
      <xsl:apply-templates/>
    </table>
    </body>
  </html>
</xsl:template>

<xsl:template match="absnode">
  <xsl:variable name="kind" select="@kind"/>
  <xsl:variable name="value" select="@value"/>
  <td valign="top">
    <table cellspacing="0">
      <tr>
        <td align="center" bgcolor="#FFAE0F" colspan="0">
          <nobr>
          <xsl:value-of select="$kind"/>
          <xsl:if test="$value != ''">
            <xsl:text> </xsl:text>
            <xsl:value-of select="$value"/>
          </xsl:if>
          </nobr>
          <br/>
          <xsl:apply-templates select="semnode"/>
          <xsl:apply-templates select="frmnode"/>
        </td>
      </tr>
      <tr>
        <td>
          <table cellspacing="0">
            <xsl:apply-templates select="absnode"/>
          </table>
        </td>
      </tr>
    </table>
  </td>
</xsl:template>

<xsl:template match="semnode">
  <xsl:variable name="kind" select="@kind"/>
  <xsl:variable name="value" select="@value"/>
  <table cellspacing="0">
    <tr>
      <td align="center" bgcolor="#91B8D2" colspan="0">
        <xsl:value-of select="$kind"/>
        <xsl:if test="$value != ''">
          <xsl:text> </xsl:text>
          <xsl:value-of select="$value"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td>
        <xsl:apply-templates select="semnode"/>
      </td>
    </tr>
  </table>
</xsl:template>

<xsl:template match="frmnode">
  <table cellspacing="0">
    <xsl:apply-templates select="frm"/>
  </table>  
</xsl:template>

<xsl:template match="frm">
  <xsl:variable name="kind" select="@kind"/>
  <xsl:variable name="value" select="@value"/>
  <tr>
    <td align="center" bgcolor="#52C461" colspan="0">
      <xsl:value-of select="$kind"/>
        <xsl:if test="$value != ''">
          <xsl:text> </xsl:text>
          <xsl:value-of select="$value"/>
        </xsl:if>
    </td>
  </tr>
</xsl:template>

</xsl:stylesheet>
