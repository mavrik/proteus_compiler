package compiler.abstree;

import java.io.*;

/** Opis konstant atomarnih tipov (in kazalcev). */
public class AbsAtomExpr extends AbsValExpr {

	public static final int BOOL   = 0;
	public static final int CHAR   = 1;
	public static final int INT    = 2;
	public static final int STRING = 3;
	public static final int VOID   = 4;
	public static final int PTR    = 5;

	/** Tip konstante. */
	public int typ_desc;

	/** Znakovna predstavitev konstante. */
	public String value;

	public AbsAtomExpr(int typ_desc, String value) {
		this.typ_desc = typ_desc;
		this.value = value;
	}

	@Override
	public void toXML(PrintStream xml) {
		String typ_name = null;
		switch (typ_desc) {
		case BOOL  : typ_name = "BOOL"  ; break;
		case CHAR  : typ_name = "CHAR"  ; break;
		case INT   : typ_name = "INT"   ; break;
		case STRING: typ_name = "STRING"; break;
		case VOID  : typ_name = "VOID"  ; break;
		case PTR   : typ_name = "PTR"   ; break;
		}
		xml.print("<absnode kind=\"" + typ_name + " CONST\" value=\"" + toXML(value) + "\">\n");
		descsToXML(xml);
		xml.print("</absnode>\n");
	}

	private String toXML(String lexeme) {
		StringBuffer lex = new StringBuffer();
		for (int i = 0; i < lexeme.length(); i++)
			switch (lexeme.charAt(i)) {
			case '\'': lex.append("&#39;"); break;
			case '\"': lex.append("&#34;"); break;
			case '&' : lex.append("&#38;"); break;
			case '<' : lex.append("&#60;"); break;
			case '>' : lex.append("&#62;"); break;
			default  :lex.append(lexeme.charAt(i)); break;
			}
		return lex.toString();
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

}
