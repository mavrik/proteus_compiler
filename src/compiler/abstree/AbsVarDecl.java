package compiler.abstree;

import java.io.*;

import compiler.frames.*;

/** Deklaracija spremenljivke (ali komponente tipa ali argumenta funkcije). */
public class AbsVarDecl extends AbsDecl {

	public AbsVarDecl(String name, AbsTypExpr typ) {
		super(name, typ);
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"VAR DECL\" value=\"" + name + "\">\n");
		descsToXML(xml);
		typ.toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

	@Override
	public void descsToXML(PrintStream xml) {
		super.descsToXML(xml);
		if (access != null) {
			xml.print("<frmnode>\n<frm kind=\"size\" value=\"" + semTyp.size() + "\"/>\n</frmnode>\n");
			access.toXML(xml);
		}
	}

	// KLICNI ZAPISI:

	public Access access;

}
