package compiler.abstree;

import java.io.*;

/** Opis unarnega izraza.  */
public class AbsUnExpr extends AbsValExpr {

	public static final int ADD = 0;
	public static final int SUB = 1;
	public static final int NOT = 2;
	public static final int VAL = 3;
	public static final int REF = 4;

	/** Vrsta binarne operacije.  */
	public int op;

	/** Podizraz.  */
	public AbsValExpr sub;

	public AbsUnExpr(int op, AbsValExpr sub) {
		this.op = op;
		this.sub = sub;
	}

	@Override
	public void toXML(PrintStream xml) {
		String op = null;
		switch (this.op) {
		case ADD: op = "+"; break;
		case SUB: op = "-"; break;
		case NOT: op = "!"; break;
		case VAL: op = "@"; break;
		case REF: op = "^"; break;
		}
		xml.print("<absnode kind=\"UN\" value=\"" + op + "\">\n");
		descsToXML(xml);
		sub.toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

}
