package compiler.abstree;

import java.io.*;
import java.util.*;

/** Seznam definicij. */
public class AbsDecls extends AbsTree {

	/** Seznam definicij. */
	public LinkedList<AbsDecl> decls;

	public AbsDecls() {
		this.decls = new LinkedList<AbsDecl>();
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"DECLS\">\n");
		descsToXML(xml);
		Iterator<AbsDecl> decls = this.decls.iterator();
		while (decls.hasNext()) decls.next().toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

}
