package compiler.frames;

import java.io.*;

import compiler.abstree.*;

/** Dostop do lokalne spremenljivke.  */
public class LocAccess extends Access {

	/** Opis spremenljivke.  */
	public AbsVarDecl var;

	/** Klicni zapis funkcije, v kateri je spremenljivka deklarirana.  */
	public Frame frame;

	/** Odmik od FPja.  */
	public int offset;

	public LocAccess(AbsVarDecl var, Frame frame) {
		this.var = var;
		this.frame = frame;
		this.offset = 0 - frame.sizeLocs - var.semTyp.size();
		frame.sizeLocs = frame.sizeLocs + var.semTyp.size();
	}

	public void toXML(PrintStream xml) {
		xml.print("<frmnode>\n<frm kind=\"loc offset\" value=\"" + offset + "\"/>\n</frmnode>\n");
	}

}
