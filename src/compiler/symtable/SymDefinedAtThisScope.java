package compiler.symtable;

/** Ob deklaraciji imena, ki je ze deklarirano na trenutem nivoju deklaracij.  */
public class SymDefinedAtThisScope extends Exception {

	private static final long serialVersionUID = 0L;

	public SymDefinedAtThisScope(String message) {
		super(message);
	}

}
