package compiler.imcode;

import java.io.*;
import java.util.*;

import compiler.report.*;

public class Main {

	/** Zaporedje delov kode.  */
	public static LinkedList<ImcChunk> chunks;

	/** Izvede prevajanje do faze izracuna vmesne kode.  */
	public static void exec() {
		/* Prevajanje do faze izracuna klicnih zapisov.  */
		compiler.frames.Main.exec();
		System.out.println("Generating intermediate code...");
		
		IMCodeGenerator code = new IMCodeGenerator();
		compiler.abstree.Main.absTree.accept(code);
		chunks = code.chunks;

		/* Izpisemo rezultat. */
		PrintStream xml = XML.open("imcode");
		Iterator<ImcChunk> chunks = Main.chunks.iterator();
		while (chunks.hasNext()) {
			ImcChunk chunk = chunks.next();
			chunk.toXML(xml);
		}
		XML.close("imcode", xml);
	}

}
