package compiler.imcode;

import java.io.*;

import compiler.frames.*;

public class ImcTEMP extends ImcExpr {

	/** Zacasna spremenljivka.  */
	public Temp temp;

	public ImcTEMP(Temp temp) {
		this.temp = temp;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<imcnode kind=\"TEMP\" value=\"" + temp.name() + "\"/>\n");
	}

	@Override
	public ImcESEQ linear() {
		return new ImcESEQ(new ImcSEQ(), this);
	}

}
