package compiler.imcode;

import java.io.*;

import compiler.frames.*;

public class ImcConstChunk extends ImcChunk {

	/** Naslov niza v pomnilniku.  */
	public Label label;

	/** Velikost niza v pomnilniku.  */
	public int size;

	/** Vrednost niza v pomnilniku.  */
	public String value;

	public ImcConstChunk(Label label, String value) {
		this.label = label;
		this.size = 4 * (value.length() + 1);
		this.value = value;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<constchunk label=\"" + label.name() + "\" value=" + value + "/>\n");
	}

}
