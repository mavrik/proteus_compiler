package compiler.abstree;

import java.io.*;

/** Izraz kot stavek.  */
public class AbsValStmt extends AbsStmt {

	/** Izraz.  */
	public AbsValExpr expr;

	public AbsValStmt(AbsValExpr expr) {
		this.expr = expr;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"VAL\">\n");
		descsToXML(xml);
		expr.toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

}
