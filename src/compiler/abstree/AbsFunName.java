package compiler.abstree;

import java.io.*;
import java.util.*;

/** Opis funkcijskega klica. */
public class AbsFunName extends AbsValExpr {

	/** Ime funkcije.  */
	public String name;

	/** Argumenti funkcije.  */
	public LinkedList<AbsValExpr> args;

	public AbsFunName(String name, LinkedList<AbsValExpr> args) {
		this.name = name;
		this.args = args;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"FUN\" value=\"" + name + "\">\n");
		descsToXML(xml);
		if (args != null) {
			Iterator<AbsValExpr> args = this.args.iterator();
			while (args.hasNext()) args.next().toXML(xml);
		}
		xml.print("</absnode>\n");
	}

	@Override
	public void descsToXML(PrintStream xml) {
		if (decl != null) xml.print("<semnode kind=\"DECL.ID:" + decl.declId + "\"/>");
		super.descsToXML(xml);
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

	// SEMANTICNA ANALIZA

	public AbsFunDecl decl;

}
