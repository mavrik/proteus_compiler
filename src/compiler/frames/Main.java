package compiler.frames;

import java.io.*;

import compiler.report.*;

public class Main {

	/** Izvede prevajanje do faze izracuna klicnih zapisov. */
	public static void exec() {
		/* Prevajanje do faze semanticne analize.  */
		compiler.semanal.Main.exec();
		System.out.println("Callculating call frames...");
		compiler.abstree.Main.absTree.accept(new FrameEvaluator());

		/* Izpisemo rezultat. */
		PrintStream xml = XML.open("frames");
		compiler.abstree.Main.absTree.toXML(xml);
		XML.close("frames", xml);
	}

}
