package compiler.lexanal;

import java.io.*;

import compiler.report.*;
import compiler.synanal.*;

%%

%class      ProteusLex
%public

%line
%column

/* Vzpostavimo zdruzljivost z orodjem Java Cup.
 * To bi lahko naredili tudi z ukazom %cup,
 * a v tem primeru ne bi mogli uporabiti razreda compiler.lexanal.TuringSym
 * namesto razreda java_cup.runtime.Symbol za opis osnovnih simbolov. */
%cupsym     compiler.synanal.ProteusTok
%implements java_cup.runtime.Scanner
%function   next_token
%type       ProteusSym
%eofval{
    return new ProteusSym(ProteusTok.EOF);
%eofval}
%eofclose

%{
	int commentLevel = 0;
	int commentStartL = 0, commentStartC = 0;

    private ProteusSym sym(int type) 
    {
        return new ProteusSym(type, yyline + 1, yycolumn + 1, yytext());
    }
%}

/* Check for open comments at the end of file */
%eof{

if (commentLevel > 0)
{
	Report.error("Start of comment without end.", commentStartL, commentStartC, -1);
}

%eof}

/* Patterns */

Integer		=	[0-9]+
Identifier  =	[A-Za-z_][A-Za-z0-9'_]*
CharLiteral = 	'([\x20-\x26\x28-\x5B\x5D-\x7E]|\\n|\\\\|\\')'
StringLiteral = \"([\x20-\x21\x23-\x5B\x5D-\x7E]|\\n|\\\\|\\\")*\"
Whitespace 	= 	[ \t\r\n]

/* State depicting inside of a comment */
%state Comment

%%

<YYINITIAL>
{
	/* Keywords */
	true				{ return sym(ProteusTok.BOOL_CONST); }
	false				{ return sym(ProteusTok.BOOL_CONST); }
	none				{ return sym(ProteusTok.NONE); }
	nil					{ return sym(ProteusTok.NIL); }
	
	bool				{ return sym(ProteusTok.BOOL); }
	char				{ return sym(ProteusTok.CHAR); }
	int					{ return sym(ProteusTok.INT); }
	string				{ return sym(ProteusTok.STRING); }
	void				{ return sym(ProteusTok.VOID); }
	arr					{ return sym(ProteusTok.ARR); }
	ptr					{ return sym(ProteusTok.PTR); }
	
	do					{ return sym(ProteusTok.DO); }
	else				{ return sym(ProteusTok.ELSE); }
	end					{ return sym(ProteusTok.END); }
	for					{ return sym(ProteusTok.FOR); }
	fun					{ return sym(ProteusTok.FUN); }
	if					{ return sym(ProteusTok.IF); }
	in					{ return sym(ProteusTok.IN); }
	let					{ return sym(ProteusTok.LET); }
	then				{ return sym(ProteusTok.THEN); }
	to					{ return sym(ProteusTok.TO); }
	typ					{ return sym(ProteusTok.TYP); }
	var					{ return sym(ProteusTok.VAR); }
	while				{ return sym(ProteusTok.WHILE); }
	
	/* Symbols */
	":"					{ return sym(ProteusTok.COLON); }
	"."					{ return sym(ProteusTok.DOT); }
	","					{ return sym(ProteusTok.COMMA); }
	";"					{ return sym(ProteusTok.SEMIC); }
	
	/* Brackets */
	"{"					{ return sym(ProteusTok.LBRACE); }
	"}"					{ return sym(ProteusTok.RBRACE); }
	"("					{ return sym(ProteusTok.LPARENT); }
	")"					{ return sym(ProteusTok.RPARENT); }
	"["					{ return sym(ProteusTok.LBRACKET); }
	"]"					{ return sym(ProteusTok.RBRACKET); }
	
	/* Operators */
	"="					{ return sym(ProteusTok.IS); }
	"+"					{ return sym(ProteusTok.ADD); }
	"-"					{ return sym(ProteusTok.SUB); }
	"*"					{ return sym(ProteusTok.MUL); }	
	"/"					{ return sym(ProteusTok.DIV); }
	"=="				{ return sym(ProteusTok.EQU); }
	"!="				{ return sym(ProteusTok.NEQ); }
	"<"					{ return sym(ProteusTok.LTH); }
	">"					{ return sym(ProteusTok.GTH); }
	">="				{ return sym(ProteusTok.GEQ); }
	"<="				{ return sym(ProteusTok.LEQ); }
	"&"					{ return sym(ProteusTok.AND); }
	"|"					{ return sym(ProteusTok.OR); }
	"!"					{ return sym(ProteusTok.NOT); }
	"^"					{ return sym(ProteusTok.VAL); }
	"@"					{ return sym(ProteusTok.REF); }
	
	/* Literals have their quotes stripped away */
	{CharLiteral}		{ return sym(ProteusTok.CHAR_CONST); }
	{StringLiteral}		{ return sym(ProteusTok.STRING_CONST);  }
	{Integer}			{ return sym(ProteusTok.INT_CONST); }
	{Identifier}		{ return sym(ProteusTok.IDENTIFIER); }
	
	/* Comment start */
	/* Remember position for more verbose error message */
	"(*"				{ commentLevel = 1; commentStartL = yyline + 1; commentStartC = yycolumn + 1; yybegin(Comment); }
}

<Comment>
{
	"(*"				{ commentLevel++; }
	"*)"				{ commentLevel--; if (commentLevel == 0) { yybegin(YYINITIAL); } }
	.					{ /* Ignore other characters */ }
}

{Whitespace}		{ /* Ignore Whitespace */ }

/* Invalid token */
.					{	Report.error("Invalid token.", yyline + 1, yycolumn + 1, -1); }