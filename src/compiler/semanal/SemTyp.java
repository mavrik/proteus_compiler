package compiler.semanal;

import compiler.report.*;

/** Opis podatkovnega tipa.  */
public abstract class SemTyp implements XMLable {

	/** Vrne dejanski tip (brez predhodnega vozlisca <code>SemNamedTyp</code>).
	 *
	 * @return Dejanski tip.
	 */
	public SemTyp actual() {
		return this;
	}

	// KLICNI ZAPISI:

	/** Velikost podatkovnega tipa v bytih.  */
	protected int size = 0;

	/** Vrne velikost podatkovenga tipa v bytih.  */
	public abstract int size();

}
