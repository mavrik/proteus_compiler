package compiler.abstree;

import java.io.*;
import java.util.*;

/** Seznam definicij. */
public class AbsStmts extends AbsTree {

	/** Seznam stavkov. */
	public LinkedList<AbsStmt> stmts;

	public AbsStmts() {
		this.stmts = new LinkedList<AbsStmt>();
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"STMTS\">\n");
		descsToXML(xml);
		Iterator<AbsStmt> stmts = this.stmts.iterator();
		while (stmts.hasNext()) stmts.next().toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

}
