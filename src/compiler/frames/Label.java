package compiler.frames;

/** Opis labele v programu.  */
public class Label {

	/** Ime labele.  */
	private String name;

	private Label(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object l) 
	{
		return name.equals(((Label)l).name);
	}
	

	@Override
	public int hashCode()
	{
		return name.hashCode();
	}

	/** Vrne ime labele.  */
	public String name() {
		return name;
	}

	private static int label_count = 0;

	/** Vrne novo anonimno labelo.  */
	public static Label newLabel() {
		return new Label("L" + (label_count++));
	}

	/** Vrne novo poimenovano labelo.  */
	public static Label newLabel(String name) {
		return new Label("_" + name);
	}

}
