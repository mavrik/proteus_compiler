package compiler.asmcode;

import java.util.*;

import compiler.frames.*;

public abstract class AsmInstr {

	/** Znakovna predstavitev ukaza.  */
	public String assem;

	/** Zacasne spremenljivke, katerih vrednosti ukaz uporabi.  */
	public LinkedList<Temp> uses;

	/** Zacasne spremenljivke, katerih vrednosti ukaz doloci.  */
	public LinkedList<Temp> defs;

	/** Seznam label, na katerih se lahko nadaljuje izvajanje ukaza.  */
	public LinkedList<Label> labels;

	protected AsmInstr(String assem, LinkedList<Temp> defs, LinkedList<Temp> uses, LinkedList<Label> labels) {
		this.assem = assem;
		this.defs = defs == null ? new LinkedList<Temp>() : defs;
		this.uses = uses == null ? new LinkedList<Temp>() : uses;
		this.labels = labels == null ? new LinkedList<Label>() : labels;
	}

	public String format(HashMap<Temp,String> map) {
		String fmtAssem = assem;
		for (int i = 0; i < uses.size(); i++) {
			Temp temp = uses.get(i);
			String regName = null;
			if (map != null) regName = map.get(temp);
			if (regName == null) regName = temp.name();
			fmtAssem = fmtAssem.replaceAll("`s" + i, regName);
		}
		for (int i = 0; i < defs.size(); i++) {
			Temp temp = defs.get(i);
			String regName = null;
			if (map != null) regName = map.get(temp);
			if (regName == null) regName = temp.name();
			fmtAssem = fmtAssem.replaceAll("`d" + i, regName);
		}
		for (int i = 0; i < labels.size(); i++) {
			Label label = labels.get(i);
			fmtAssem = fmtAssem.replaceAll("`l" + i, label.name());
		}
		return fmtAssem;
	}

}
