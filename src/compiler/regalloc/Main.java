package compiler.regalloc;

import java.util.*;

import compiler.frames.*;
import compiler.imcode.*;
import compiler.asmcode.*;

public class Main {

	public static LinkedList<String> asmProgram;
	
	/** Izvede prevajanje do faze izracuna interferen"cnega grafa.  */
	public static void exec() {
		/* Prevajanje do faze izracuna linearizirane vmesne kode.  */
		compiler.lincode.Main.exec();
		System.out.println("Allocating registers...");
		/* Testni izpis generirane kode za vse funkcije.  */
		asmProgram = new LinkedList<String>();
		
		Iterator<ImcChunk> chunks = compiler.imcode.Main.chunks.iterator();
		while (chunks.hasNext()) 
		{
			ImcChunk chunk = chunks.next();
			LinkedList<String> outputCode = generateFinalASM(chunk);
			
			asmProgram.addAll(outputCode);
		}
	}

	/** Izracuna preslikavo zacasnih spremenljivk v registre.
	 * 	Pri tem po potrebi opravi preliv dolocenih zacasnih spremenljivk v klicni zapis.
	 *
	 * @param asmCode Zaporedje strojnih ukazov.
	 * @return Preslikava zacasnih spremenljivk v registre.
	 * */
	public static HashMap<Temp,String> allocRegs(LinkedList<AsmInstr> asmCode) 
	{
		RegistryAllocator allocator = new RegistryAllocator(asmCode);
		
		return allocator.getTempToRegMapping();
	}
	
	private static LinkedList<String> generateFinalASM(ImcChunk codeChunk)
	{ 
		LinkedList<String> code = new LinkedList<String>();
		
		// Add prologue and epilogue to the function
		if (codeChunk instanceof ImcCodeChunk)
		{
			ImcCodeChunk chunk = (ImcCodeChunk)codeChunk;
			code.add(".text");
			code.add(".align 2");
			code.add(".global " + ((ImcCodeChunk)codeChunk).frame.label.name());
			code.add(".type " + ((ImcCodeChunk)codeChunk).frame.label.name() + ",%function");
			
			LinkedList<AsmInstr> asmCode = compiler.asmcode.Main.generateAsmCode(chunk.lincode, chunk.frame);
			HashMap<Temp,String> tmpToRegMap = allocRegs(asmCode);
			
			for (AsmInstr instr : asmCode)
			{
				code.add(instr.format(tmpToRegMap));
			}
		}
		else if (codeChunk instanceof ImcDataChunk)
		{
			ImcDataChunk chunk = (ImcDataChunk)codeChunk;
			code.add(".data");
			code.add(".align 2");
			code.add(".global " + chunk.label.name());
			code.add(".type " + chunk.label.name() + ",%object");
			code.add(".size " + chunk.label.name() + ",4");
			code.add(chunk.label.name() + ": " + ".word 0");
		}
		
		return code;
	}
}
