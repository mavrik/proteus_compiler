package compiler.asmcode;

import java.util.*;

import compiler.frames.*;

public class AsmOPER extends AsmInstr {

	public AsmOPER(String assem, LinkedList<Temp> defs, LinkedList<Temp> uses, LinkedList<Label> labels) {
		super(assem, defs, uses, labels);
	}

	public AsmOPER(String assem, LinkedList<Temp> defs, LinkedList<Temp> uses) {
		super(assem, defs, uses, null);
	}

}
