.text
.align 2
.global _main
.type _main,%function
mov r1, [r0]
mov r2, #1
; Store static link
str r0, [sp]
; Store arguments
str r1, [sp, #4]
str r2, [sp, #8]
b _print_int
mov r0, [r0]
