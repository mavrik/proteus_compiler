package compiler.regalloc;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;

import compiler.asmcode.AsmInstr;
import compiler.frames.Temp;
import compiler.liveness.LivenessAnalyser;
import compiler.liveness.LivenessEdge;

public class RegistryAllocator
{
	private static final int NUM_REGISTERS = 10; 
	
	private static class GraphNode
	{
		private Temp var;
		private LinkedList<GraphNode> connections = new LinkedList<GraphNode>();
		
		private int color = -1;
		private boolean spillTag = false;
		
		private boolean active = true;
		
		public GraphNode(Temp t)
		{
			this.var = t;
		}
		
		public void addConnection(GraphNode g)
		{
			connections.add(g);
		}
		
		public int getDegree()
		{
			int degree = 0;
			
			for (GraphNode connection : connections)
			{
				if (connection.isActive())
				{
					degree++;
				}
			}
			
			return degree;
		}
		
		/**
		 * Finds a "free" color from nearby nodes and colors itself accordingly
		 */
		public void setColor()
		{
			Integer colors[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
			HashSet<Integer> availableColors = new HashSet<Integer>(Arrays.asList(colors));
			
			// Find free color
			for (GraphNode connection : connections)
			{
				if (connection.isActive() && connection.getColor() != -1)
				{
					availableColors.remove(connection.getColor());
				}
			}
			
			// Only free colors remaining, grab one
			this.color = availableColors.iterator().next();
		}
		
		public int getColor()
		{
			return this.color;
		}

		public Temp getVar()
		{
			return this.var;
		}
		
		@Override
		public boolean equals(Object o)
		{
			GraphNode g = (GraphNode)o;
			return this.var.equals(g.getVar());
		}

		@Override
		public int hashCode()
		{
			return this.var.hashCode();
		}

		public boolean isActive()
		{
			return active;
		}

		public void setActive(boolean active)
		{
			this.active = active;
		}
		
		
	}
	
	private LinkedList<AsmInstr> asmCode = null;
	private HashMap<Temp, GraphNode> graph = null;
	
	public RegistryAllocator(LinkedList<AsmInstr> asmCode)
	{
		this.asmCode = asmCode;		
	}
	
	public HashMap<Temp, String> getTempToRegMapping()
	{
		HashMap<Temp, String> mapping = new HashMap<Temp, String>();
		
		// Get current interference graph
		buildInterferenceGraph();
		
		// Add missing nodes
		for (AsmInstr instr : asmCode)
		{
			for (Temp def : instr.defs)
			{
				if (!graph.containsKey(def))
					graph.put(def, new GraphNode(def));
			}
			
			for (Temp use : instr.uses)
			{
				if (!graph.containsKey(use))
					graph.put(use, new GraphNode(use));
			}
		}
		
		// SIMPLIFY
		LinkedList<GraphNode> nodeStack = new LinkedList<GraphNode>();
		
		// Queue holding active nodes
		PriorityQueue<GraphNode> activeNodes = new PriorityQueue<GraphNode>(graph.size(), new Comparator<GraphNode>()
				{
					@Override
					public int compare(GraphNode o1, GraphNode o2)
					{
						return o1.getDegree() - o2.getDegree();
					};
				});
		
		
		// Fill priority queue
		for (GraphNode node : graph.values())
		{	
			activeNodes.add(node);
			node.setActive(true);
		}
		
		// Fill stack with nodes with lesser degree than number of registers
		while (!activeNodes.isEmpty() && activeNodes.peek().getDegree() < NUM_REGISTERS)
		{
			GraphNode node = activeNodes.poll();
			nodeStack.add(node);
			node.setActive(false);
		}
		
		for (GraphNode node : activeNodes)
		{
			System.err.println(" POTENTIAL SPILL " + node.var.name());
		}
		
		// SIMPLIFY DONE
		
		// DO SPILL
		
		// SELECT
		while(!nodeStack.isEmpty())
		{
			GraphNode node = nodeStack.poll();
			node.setActive(true);
			node.setColor();
		}
		
		// DO MAPPING
		for (GraphNode node : graph.values())
		{
			mapping.put(node.getVar(), "r" + node.getColor());
		}
		
		return mapping;
	}
	
	private void buildInterferenceGraph()
	{
		graph = new HashMap<Temp, GraphNode>();
	
		LinkedList<LivenessEdge> livenessLinks = LivenessAnalyser.analyze(asmCode);
		
		for (LivenessEdge link : livenessLinks)
		{
			// Create nodes if they don't exist
			if (!graph.containsKey(link.getT0()))
			{
				GraphNode node = new GraphNode(link.getT0());
				graph.put(link.getT0(), node);
			}
			
			if (!graph.containsKey(link.getT1()))
			{
				GraphNode node = new GraphNode(link.getT1());
				graph.put(link.getT1(), node);
			}
			
			// Connect nodes
			graph.get(link.getT0()).addConnection(graph.get(link.getT1()));
			graph.get(link.getT1()).addConnection(graph.get(link.getT0()));
		}
	}
}
