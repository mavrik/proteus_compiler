package compiler.abstree;

import java.io.*;

import compiler.frames.*;

/** Deklaracija funkcije. */
public class AbsFunDecl extends AbsDecl {

	/** Argumenti funkcije. */
	public AbsDecls args;

	/** Jedro funkcije. */
	public AbsValExpr val;

	public AbsFunDecl(String name, AbsDecls args, AbsTypExpr typ, AbsValExpr val) {
		super(name, typ);
		this.args = args;
		this.val = val;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"FUN DECL\" value=\"" + name + "\">\n");
		descsToXML(xml);
		if (args != null) args.toXML(xml);
		typ.toXML(xml);
		val.toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

	@Override
	public void descsToXML(PrintStream xml) {
		super.descsToXML(xml);
		if (frame != null) frame.toXML(xml);
	}

	// KLICNI ZAPISI:

	public Frame frame = null;

}
