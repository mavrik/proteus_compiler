package compiler.semanal;

import java.io.*;

import compiler.report.Report;

public class SemNamedTyp extends SemTyp {

	/** Ime.  */
	public String name;

	/** Tip.  */
	public SemTyp typ;

	public SemNamedTyp(String name, SemTyp typ) {
		this.name = name;
		this.typ = typ;
	}

	@Override
	public SemTyp actual() {
		return typ.actual();
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<semnode kind=\"NAMED TYP\" value=\"" + name + "\"/>\n");
	}

	@Override
	public int size() {
		if (size == -1) Report.error("Cannot establish the size of a type.", 1);
		if (size == 0) {
			size = -1;
			size = typ.size();
		}
		return size;
	}

}
