package compiler.semanal;

import java.io.*;

/** Opis kazalcev. */
public class SemPtrTyp extends SemTyp {

	/** Tip podatka, na katereka kaze kazalec. */
	public SemTyp base;

	public SemPtrTyp(SemTyp base) {
		this.base = base;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<semnode kind=\"PTR\">\n");
		if (base != null) base.toXML(xml);
		xml.print("</semnode>\n");
	}

	@Override
	public int size() {
		size = 4;
		return size;
	}

}
