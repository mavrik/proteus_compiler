package compiler.semanal;

import java.io.*;

import compiler.report.*;

/** Opis tabel. */
public class SemArrTyp extends SemTyp {

	/** Dimenzija tabele. */
	public int dim;

	/** Tip elementa tabele. */
	public SemTyp base;

	public SemArrTyp(int dim, SemTyp base) {
		this.dim = dim;
		this.base = base;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<semnode kind=\"ARR[" + dim + "]\">\n");
		base.toXML(xml);
		xml.print("</semnode>\n");
	}

	@Override
	public int size() {
		if (size == -1) Report.error("Cannot establish the size of a type.", 1);
		if (size == 0) {
			size = -1;
			size = dim * base.size();
		}
		return size;
	}
}
