package compiler.semanal;

import java.util.HashSet;

import compiler.abstree.AbsArrTyp;
import compiler.abstree.AbsAtomExpr;
import compiler.abstree.AbsAtomTyp;
import compiler.abstree.AbsBinExpr;
import compiler.abstree.AbsDecl;
import compiler.abstree.AbsDecls;
import compiler.abstree.AbsForStmt;
import compiler.abstree.AbsFunDecl;
import compiler.abstree.AbsFunName;
import compiler.abstree.AbsIfStmt;
import compiler.abstree.AbsLetExpr;
import compiler.abstree.AbsPtrTyp;
import compiler.abstree.AbsRecTyp;
import compiler.abstree.AbsStmt;
import compiler.abstree.AbsStmts;
import compiler.abstree.AbsTypDecl;
import compiler.abstree.AbsTypName;
import compiler.abstree.AbsUnExpr;
import compiler.abstree.AbsValExpr;
import compiler.abstree.AbsValStmt;
import compiler.abstree.AbsVarDecl;
import compiler.abstree.AbsVarName;
import compiler.abstree.AbsWhileStmt;
import compiler.abstree.Visitor;
import compiler.report.Report;

public class TypeResolver implements Visitor 
{
	private boolean preprocessing = false;
	
	/**
	 * Parses expression tree for array dimensions
	 * @param dim top node of dimension array tree
	 * @return
	 */
	private int calculateArrayDim(AbsValExpr dim)
	{
		if (dim instanceof AbsAtomExpr && ((AbsAtomExpr)dim).typ_desc == AbsAtomExpr.INT)
		{
			return Integer.parseInt(((AbsAtomExpr)dim).value);
		}
		else if (dim instanceof AbsUnExpr && ((AbsUnExpr)dim).op == AbsUnExpr.ADD)
		{
			return calculateArrayDim(((AbsUnExpr)dim).sub);
		}
		else if (dim instanceof AbsBinExpr)
		{
			AbsBinExpr expr = (AbsBinExpr)dim;
			
			switch(expr.op)
			{
				case AbsBinExpr.ADD:
					return calculateArrayDim(expr.lsub) + calculateArrayDim(expr.rsub);
				case AbsBinExpr.SUB:
					return calculateArrayDim(expr.lsub) - calculateArrayDim(expr.rsub);
				case AbsBinExpr.MUL:
					return calculateArrayDim(expr.lsub) * calculateArrayDim(expr.rsub);
				case AbsBinExpr.DIV:
					return calculateArrayDim(expr.lsub) / calculateArrayDim(expr.rsub);
				default:
					Report.error("Invalid array dimension expression!", dim.begLine, dim.begColumn, 7);
			}
		}
		else
		{
			Report.error("Invalid array dimension expression!", dim.begLine, dim.begColumn, 7);
		}
		
		return 0;
	}
	
	@Override
	public Object visit(AbsArrTyp node)
	{
		if (preprocessing)
		{
			node.base.accept(this);
			node.dim.accept(this);
			node.semTyp = new SemArrTyp(calculateArrayDim(node.dim), node.base.semTyp);
			
			return null;
		}
		
		node.base.accept(this);
		
		return null;
	}

	@Override
	public Object visit(AbsAtomExpr node)
	{
		if (node.semTyp == null)
		{
			if (node.typ_desc == AbsAtomExpr.PTR)
			{
				node.semTyp = new SemPtrTyp(null);
			}
			else
			{
				node.semTyp = new SemAtomTyp(node.typ_desc);
			}
		}
		
		return null;
	}

	@Override
	public Object visit(AbsAtomTyp node)
	{
		if (node.semTyp == null)
			node.semTyp = new SemAtomTyp(node.typ_desc);
		
		return null;
	}

	@Override
	public Object visit(AbsBinExpr node)
	{
		node.lsub.accept(this);
		
		// Right side for DOT will be assigned when the record type is resolved
		if (node.op != AbsBinExpr.DOT)
			node.rsub.accept(this);
		
		
		switch(node.op)
		{
			// INTEGER operators
			case AbsBinExpr.ADD:
			case AbsBinExpr.SUB:
			case AbsBinExpr.DIV:
			case AbsBinExpr.MUL:
				if (!cmpAtomType(node.lsub.semTyp, SemAtomTyp.INT) ||
					!cmpAtomType(node.rsub.semTyp, SemAtomTyp.INT))
				{
					Report.error("Arithmetic binary operator requires INT parameters!", node.begLine, node.begColumn, 10);
				}
				
				node.semTyp = node.lsub.semTyp;
				break;
			
			// BOOLEAN operators
			case AbsBinExpr.OR:
			case AbsBinExpr.AND:
				if (!cmpAtomType(node.lsub.semTyp, SemAtomTyp.BOOL) ||
					!cmpAtomType(node.rsub.semTyp, SemAtomTyp.BOOL))
				{
					Report.error("Boolean binary operator requires BOOL parameters!", node.begLine, node.begColumn, 11);
				}
				
				node.semTyp = node.lsub.semTyp;
				break;
				
			case AbsBinExpr.DOT:
				// On the left has to be a record type
				if (!(node.lsub.semTyp.actual() instanceof SemRecTyp))
				{
					Report.error("Dot used on a non-record type.", node.begLine, node.begColumn, 11);
				}
				
				if (!(node.rsub instanceof AbsVarName))
				{
					Report.error("Errorenus record acces.", node.begLine, node.begColumn, 12);
				}
				
				// Find record inside
				SemRecTyp recordType  = (SemRecTyp)node.lsub.semTyp.actual();
				String varName = ((AbsVarName)node.rsub).name;
				
				for (AbsVarDecl dec : recordType.comps)
				{
					if (dec.name.equals(varName))
					{
						node.semTyp = dec.semTyp;
						node.rsub.semTyp = dec.semTyp;
						
						return null;
					}
				}
				
				Report.error("Unknown record variable.", node.begLine, node.begColumn, 13);
				
				break;	
				
			case AbsBinExpr.IS:
				// Assignment operators have to be of same types
				if (!compareTypes(node.lsub.semTyp, node.rsub.semTyp))
				{
					Report.error("Assignment of incompatible types.", 14);
				}
				
				node.semTyp = new SemAtomTyp(SemAtomTyp.VOID);
				
				break;
			
			case AbsBinExpr.GTH:
			case AbsBinExpr.GEQ:
			case AbsBinExpr.LEQ:
			case AbsBinExpr.LTH:
			case AbsBinExpr.EQU:
			case AbsBinExpr.NEQ:
				if (!compareTypes(node.lsub.semTyp, node.rsub.semTyp))
				{
					Report.error("Comparation of incompatible types.", 15);
				}
				
				node.semTyp = new SemAtomTyp(SemAtomTyp.BOOL);
				
				break;
				
			case AbsBinExpr.ARR:
				if (!(node.lsub.semTyp instanceof SemArrTyp) &&
					!cmpAtomType(node.rsub.semTyp, SemAtomTyp.INT))
				{
					Report.error("Invalid array access expression!", node.begLine, node.begColumn, 16);
				}
				
				node.semTyp = ((SemArrTyp)(node.lsub.semTyp.actual())).base;
				break;
		} 
		
		return null;
	}

	@Override
	public Object visit(AbsDecls node)
	{
		preprocessing = true;
		
		// Preprocess types
		for (AbsDecl decl : node.decls)
		{
			if (decl instanceof AbsTypDecl)
			{
				decl.accept(this);
			}
		}
		
		// Preprocess functions
		for (AbsDecl decl : node.decls)
		{
			if (decl instanceof AbsFunDecl)
			{
				decl.accept(this);
			}
		}
		
		preprocessing = false;
		
		// Functions and types were preprocessed, completly resolve types again
		for (AbsDecl decl : node.decls)
		{
			if (decl instanceof AbsTypDecl)
			{
				decl.accept(this);
			}
		}
		
		for (AbsDecl decl : node.decls)
		{
			if (!(decl instanceof AbsTypDecl))
			{
				decl.accept(this);
			}
		}
		
		return null;
	}

	@Override
	public Object visit(AbsForStmt node)
	{
		node.expr.accept(this);
		node.hi.accept(this);
		node.lo.accept(this);
		node.stmts.accept(this);
		
		// TODO: type check
		
		return null;
	}

	@Override
	public Object visit(AbsFunDecl node)
	{
		if (preprocessing)
		{
			node.typ.accept(this);
			SemFunTyp typ = new SemFunTyp(node.typ.semTyp);
			
			// Check arguments
			for (AbsDecl decl : node.args.decls)
			{
				decl.accept(this);
				typ.args.add(decl.semTyp);
			}
			
			node.semTyp = typ;
			
			return null;
		}
		
		node.typ.accept(this);
		
		// Recheck variable argument types
		SemFunTyp nodeTyp = (SemFunTyp)node.semTyp;
		nodeTyp.args.clear();
		
		for (AbsDecl decl : node.args.decls)
		{
			decl.accept(this);
			nodeTyp.args.add(decl.semTyp);
		}
		
		node.val.accept(this);
		
		return null;
	}

	@Override
	public Object visit(AbsFunName node)
	{
		node.semTyp = ((SemFunTyp)node.decl.semTyp).result;
		
		if (node.args != null)
		{
			for (AbsValExpr ex : node.args)
			{
				ex.accept(this);
			}
		}
		
		return null;
	}

	@Override
	public Object visit(AbsIfStmt node)
	{
		node.cond.accept(this);
		node.then_stmts.accept(this);
		
		if (node.else_stmts != null)
			node.else_stmts.accept(this);
		
		// Check type of condition
		
		if (!cmpAtomType(node.cond.semTyp, SemAtomTyp.BOOL))
		{
			Report.error("Not a BOOL condition in IF.", node.begLine, node.begColumn, 16);
		}
		
		return null;
	}

	@Override
	public Object visit(AbsLetExpr node)
	{
		// Check declarations first
		if (node.defs != null)
		{
			node.defs.accept(this);
		}
		
		if (node.stmts != null)
		{
			node.stmts.accept(this);
		}
		
		node.expr.accept(this);
		node.semTyp = node.expr.semTyp;
		
		return null;
	}

	@Override
	public Object visit(AbsPtrTyp node)
	{		
		if (preprocessing)
		{
			node.base.accept(this);
			node.semTyp = new SemPtrTyp(node.base.semTyp);
			
			return null;
		}
		
		if (node.base != null)
		{
			node.base.accept(this);
			
			if (node.semTyp == null)
			{
				node.semTyp = new SemPtrTyp(node.base.semTyp);
			}
			
			((SemPtrTyp)node.semTyp).base = node.base.semTyp;
		}
		
		return null;
	}

	@Override
	public Object visit(AbsRecTyp node)
	{
		if (node.semTyp == null)
		{
			SemRecTyp typ = new SemRecTyp();
			
			for (AbsVarDecl components : node.comps)
			{
				typ.comps.add(components);
			}
			
			node.semTyp = typ;
		}
		
		// Travel over all types and check them
		for (AbsVarDecl comp : ((SemRecTyp)node.semTyp).comps)
		{
			comp.accept(this);
		} 
		
		return null; 
	}

	@Override
	public Object visit(AbsStmts node)
	{
		for (AbsStmt stmt : node.stmts)
		{
			stmt.accept(this);
		}
		
		return null;
	}

	@Override
	public Object visit(AbsTypDecl node)
	{	
		node.typ.accept(this);
		
		if (node.semTyp == null)
		{
			node.semTyp = new SemNamedTyp(node.name, node.typ.semTyp);
		}
		
		return null;
	}

	@Override
	public Object visit(AbsTypName node)
	{
		if (preprocessing)
		{
			node.semTyp = new SemNamedTyp(node.name, null);
			
			return null;
		}
		
		// Case could happen where the type name wasn't visited in preprocessing stage, like when the type isn't in a declaration
		if (node.semTyp == null)
		{
			node.semTyp = new SemNamedTyp(node.name, null);
		}
		
		((SemNamedTyp)node.semTyp).typ = node.decl.semTyp;
		
		return null;
	}

	@Override
	public Object visit(AbsUnExpr node)
	{
		node.sub.accept(this);
		
		switch(node.op)
		{
			case AbsUnExpr.ADD:
			case AbsUnExpr.SUB:
				if (!cmpAtomType(node.sub.semTyp, AbsAtomTyp.INT))
				{
					Report.error("Arithmetic unary expression needs argument of INT type!", node.begLine, node.begColumn, 9);
				}
				
				node.semTyp = node.sub.semTyp;
				break;
				
			case AbsUnExpr.NOT:
				if (!cmpAtomType(node.sub.semTyp, AbsAtomTyp.BOOL))
				{
					Report.error("Not operator needs argument of BOOL type!", node.begLine, node.begColumn, 9);
				}
				break;
			
			case AbsUnExpr.VAL:
				if (!(node.sub.semTyp.actual() instanceof SemPtrTyp))
				{
					Report.error("Value of operator requires a pointer type!", node.begLine, node.begColumn, 10);
				}
				
				node.semTyp = ((SemPtrTyp)node.sub.semTyp.actual()).base;
				return null;
			
			case AbsUnExpr.REF:
				node.semTyp = new SemPtrTyp(node.sub.semTyp);
				return null;
		}
		
		node.semTyp = node.sub.semTyp;
		
		return null;
	}

	@Override
	public Object visit(AbsValStmt node)
	{
		node.expr.accept(this);
		
		return null;
	}

	@Override
	public Object visit(AbsVarDecl node)
	{
		node.typ.accept(this);
		node.semTyp = node.typ.semTyp;
		
		return null;
	}

	@Override
	public Object visit(AbsVarName node)
	{
		node.decl.accept(this);
		node.semTyp = node.decl.semTyp;
		
		return null;
	}

	@Override
	public Object visit(AbsWhileStmt node)
	{
		node.cond.accept(this);
		
		for (AbsStmt stmt: node.stmts.stmts)
		{
			stmt.accept(this);
		}
		
		node.semTyp = new SemAtomTyp(SemAtomTyp.VOID);
		
		return null;
	}
	
	/**
	 * Checks if a type is an atom type with certain desc
	 * @param node
	 * @param type
	 * @return
	 */
	private boolean cmpAtomType(SemTyp node, int type)
	{
		if (node instanceof SemAtomTyp && ((SemAtomTyp)node.actual()).typ_desc == type)
			return true;
		
		return false;
	}
	
	private HashSet<String> typCmpVisitedMap = new HashSet<String>();
	
	private boolean compareTypes(SemTyp node1, SemTyp node2)
	{
		// Prevent infinite recursion by persuming already visited type pair as the same
		if (node1 instanceof SemNamedTyp && node2 instanceof SemNamedTyp)
		{
			if (typCmpVisitedMap.contains(((SemNamedTyp)node1).name) &&
				typCmpVisitedMap.contains(((SemNamedTyp)node2).name))
			{
				return true;
			}
			
			typCmpVisitedMap.add(((SemNamedTyp)node1).name);
			typCmpVisitedMap.add(((SemNamedTyp)node2).name);
		}
		
		SemTyp type1 = node1.actual();
		SemTyp type2 = node2.actual();
		
		
		// Atomic types
		if (type1 instanceof SemAtomTyp &&
			type2 instanceof SemAtomTyp)
		{
			return cmpAtomType(type1, ((SemAtomTyp)type2).typ_desc);
		}
		// Arrays
		else if (type1 instanceof SemArrTyp &&
				 type2 instanceof SemArrTyp)
		{
			return (((SemArrTyp)type1).dim == ((SemArrTyp)type2).dim) && compareTypes(((SemArrTyp)type1).base, ((SemArrTyp)type2).base); 
		}
		else if (type1 instanceof SemPtrTyp &&
				 type2 instanceof SemPtrTyp)
		{
			// Check NIL
			if (((SemPtrTyp)type1).base == null)
			{
				Report.error("Tried to assign a pointer to NIL constant.", 17);
			}
			
			if (((SemPtrTyp)type2).base == null)
			{
				return true;
			}
			
			return compareTypes(((SemPtrTyp)type1).base, ((SemPtrTyp)type2).base);
		}
		else if (type1 instanceof SemRecTyp &&
				 type2 instanceof SemRecTyp)
		{
			// Check by components
			SemRecTyp recType1 = (SemRecTyp)type1;
			SemRecTyp recType2 = (SemRecTyp)type2;
			
			// Same number of components
			if (recType1.comps.size() != recType2.comps.size())
			{
				return false;
			}
			
			for (int i = 0; i < recType1.comps.size(); i++)
			{
				if (!compareTypes(recType1.comps.get(i).semTyp, recType2.comps.get(i).semTyp))
				{
					return false;
				}
			}
			
			return true;
		}
		
		if (node1 instanceof SemNamedTyp && node2 instanceof SemNamedTyp)
		{
			typCmpVisitedMap.remove(((SemNamedTyp)node1).name);
			typCmpVisitedMap.remove(((SemNamedTyp)node2).name);
		}
		
		return false;
	}
}