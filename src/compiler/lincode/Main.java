package compiler.lincode;

import java.io.PrintStream;
import java.util.Iterator;

import compiler.imcode.ImcChunk;
import compiler.imcode.ImcCodeChunk;
import compiler.report.XML;

public class Main {

	/** Izvede prevajanje do faze izracuna linearne vmesne kode.  */
	public static void exec() {
		/* Prevajanje do faze izracuna vmesne kode.  */
		compiler.imcode.Main.exec();
		System.out.println("Linearizing code...");
		/* Izpisemo rezultat. */
		PrintStream xml = XML.open("lincode");
		Iterator<ImcChunk> chunks = compiler.imcode.Main.chunks.iterator();
		while (chunks.hasNext()) {
			ImcChunk chunk = chunks.next();
			if (chunk instanceof ImcCodeChunk) {
				ImcCodeChunk codeChunk = (ImcCodeChunk)chunk;
				codeChunk.lincode = codeChunk.imcode.linear();
			}
			chunk.toXML(xml);
		}
		XML.close("lincode", xml);

		System.out.println("\n==========================================");
		Interpreter interpreter = new Interpreter(compiler.imcode.Main.chunks);
		System.out.println("\n==========================================");
	}

}
