package compiler.semanal;

import java.io.*;
import java.util.*;

import compiler.abstree.*;
import compiler.report.Report;

/** Opis zapisov. */
public class SemRecTyp extends SemTyp {

	/** Opis tipov posameznih komponent. */
	public LinkedList<AbsVarDecl> comps;

	public SemRecTyp() {
		this.comps = new LinkedList<AbsVarDecl>();
	}

	/** Doda novo komponento v opis zapisa.
	 *
	 * @param comp Opis nove komponente.
	 * @return Dodana komponenta ali <code>null</code>, ce komponenta s tem imenom ze obstaja.
	 */
	public AbsVarDecl add(AbsVarDecl comp) {
		if (get(comp.name) == null) {
			comps.add(comp);
			return comp;
		}
		return null;
	}

	/** Vrne opis komponente zapisa.
	 *
	 * @param name Ime iskane komponente.
	 * @return Opis iskane komponente ali <code>null</code>, ce komponenta s tem imenom ne obstaja.
	 */
	public AbsVarDecl get(String name) {
		Iterator<AbsVarDecl> comps = this.comps.iterator();
		while (comps.hasNext()) {
			AbsVarDecl comp = comps.next();
			if (comp.name.equals(name)) return comp;
		}
		return null;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<semnode kind=\"REC\">\n");
		Iterator<AbsVarDecl> comps = this.comps.iterator();
		while (comps.hasNext())
			comps.next().semTyp.toXML(xml);
		xml.print("</semnode>\n");
	}

	@Override
	public int size() {
		if (size == -1) Report.error("Cannot establish the size of a type(1).", 1);
		if (size == 0) {
			size = -1;
			int sum = 0;
			Iterator<AbsVarDecl> comps = this.comps.iterator();
			while (comps.hasNext()) {
				AbsVarDecl comp = comps.next();
				sum = sum + comp.semTyp.size();
			}
			size = sum;
		}
		return size;
	}
}
