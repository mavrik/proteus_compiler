package compiler.asmcode;

import compiler.frames.*;

public class AsmMOVE extends AsmInstr {

	public AsmMOVE(String assem, Temp use, Temp def) {
		super(assem, null, null, null);
		defs.add(def);
		uses.add(use);
	}

}
