package compiler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import compiler.report.Report;

public class Main {

	/** Ime programa, ki ga prevajamo.  */
	public static String prgName;

	/**
	 * Zaganjac prevajalnika.
	 *
	 * Prvi argument v ukazni vrstici vsebuje ime vhodne datoteke z izvorno kodo
	 * v programskem jeziku Proteus. Drugi argument (ce je prisoten) doloca
	 * zadnjo fazo prevajanja, ki jo se opravimo. Ostale argumente ignoriramo.
	 *
	 * @param argv
	 *            Argumenti ukazne vrstice.
	 */
	public static void main(String[] args) {
		System.out.println("This is Proteus compiler:");

		/* Dolocimo ime programa, ki ga prevajamo.  */
		if (args.length < 1) {
			Report.error("Source file is not specified.", 1);
		}
		else prgName = args[0];

		/* Dolocimo zadnjo fazo prevajanja.  */
		String phase = args.length < 2 ? "" : args[1];
		/* Opravimo izbrano fazo prevajanja (in vse predhodne).  */
		if (phase.equals("lexanal")) compiler.lexanal.Main.exec(); else
		if (phase.equals("synanal")) compiler.synanal.Main.exec(); else
		if (phase.equals("abstree")) compiler.abstree.Main.exec(); else
		if (phase.equals("semanal")) compiler.semanal.Main.exec(); else
		if (phase.equals("frames" )) compiler.frames.Main.exec(); else
		if (phase.equals("imcode" )) compiler.imcode.Main.exec(); else
		if (phase.equals("lincode" )) compiler.lincode.Main.exec(); else
		if (phase.equals("asmcode" )) compiler.asmcode.Main.exec(); else
		if (phase.equals("liveness")) compiler.liveness.Main.exec(); else
		if (phase.equals("regalloc")) compiler.regalloc.Main.exec(); else
		compiler.regalloc.Main.exec();

		try
		{
			PrintWriter writer = new PrintWriter(new File(prgName + ".s"));
			
			for (String codeLine : compiler.regalloc.Main.asmProgram)
			{
				writer.println(codeLine);
			}
			
			writer.close();
		} 
		catch (IOException e)
		{}
		
		System.out.print(":-) Done.\n");
		System.exit(0);
	}
}
