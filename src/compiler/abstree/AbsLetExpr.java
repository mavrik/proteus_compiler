package compiler.abstree;

import java.io.*;

/** LET ... END izraz.  */
public class AbsLetExpr extends AbsValExpr {

	/** Vgnezdene definicije.  */
	public AbsDecls defs;

	/** Vgnezdeni stavki. */
	public AbsStmts stmts;

	/** Izraz.  */
	public AbsValExpr expr;

	public AbsLetExpr(AbsDecls defs, AbsStmts stmts, AbsValExpr expr) {
		this.defs = defs;
		this.stmts = stmts;
		this.expr = expr;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"LET\">\n");
		descsToXML(xml);
		if (defs != null) defs.toXML(xml);
		if (stmts != null) stmts.toXML(xml);
		expr.toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

}
