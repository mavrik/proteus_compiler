package compiler.abstree;

import java.io.*;

/** Opis FOR zanke. */
public class AbsForStmt extends AbsStmt {

	/** Zancna spremenljivka. */
	public AbsValExpr expr;

	/** Spodnja meja. */
	public AbsValExpr lo;

	/** Zgornja meja. */
	public AbsValExpr hi;

	/** Jedro zanke. */
	public AbsStmts stmts;

	public AbsForStmt(AbsValExpr expr, AbsValExpr lo, AbsValExpr hi, AbsStmts stmts) {
		this.expr = expr;
		this.lo = lo;
		this.hi = hi;
		this.stmts = stmts;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"FOR\">\n");
		descsToXML(xml);
		expr.toXML(xml);
		lo.toXML(xml);
		hi.toXML(xml);
		stmts.toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

}
