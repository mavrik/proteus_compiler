package compiler.abstree;

import java.io.*;

/** Opis spremenljivke v izrazu. */
public class AbsVarName extends AbsValExpr {

	/** Ime spremenljivke.  */
	public String name;

	public AbsVarName(String name) {
		this.name = name;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"VAR\" value=\"" + name + "\">\n");
		descsToXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public void descsToXML(PrintStream xml) {
		if (decl != null) xml.print("<semnode kind=\"DECL.ID:" + decl.declId + "\"/>");
		super.descsToXML(xml);
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

	// SEMANTICNA ANALIZA

	public AbsVarDecl decl;

}
