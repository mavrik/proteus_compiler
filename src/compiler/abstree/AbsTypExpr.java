package compiler.abstree;

/**
 * Opis tipa.
 *
 * @see AbsAtomTyp
 * @see AbsArrTyp
 * @see AbsPtrTyp
 * @see AbsRecTyp
 * @see AbsTypName
 */
public abstract class AbsTypExpr extends AbsTree {

}
