package compiler.abstree;

import java.io.*;

/** Opis pogojnega stavka. */
public class AbsIfStmt extends AbsStmt {

	/** Pogoj. */
	public AbsValExpr cond;

	/** Pozitivna veja. */
	public AbsStmts then_stmts;

	/** Negativna veja. */
	public AbsStmts else_stmts;

	public AbsIfStmt(AbsValExpr cond, AbsStmts then_stmts, AbsStmts else_stmts) {
		this.cond = cond;
		this.then_stmts = then_stmts;
		this.else_stmts = else_stmts;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"IF\">\n");
		descsToXML(xml);
		cond.toXML(xml);
		then_stmts.toXML(xml);
		if (else_stmts != null) else_stmts.toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

}
