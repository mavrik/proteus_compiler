package compiler.lexanal;

import java.io.*;
import java.lang.reflect.*;

import compiler.report.*;
import compiler.synanal.*;

public class Main {

	public static String[] proteusTermNames;

	static {
		/* Pripravimo imena vrst koncnih simbolov.  */
		ProteusTok proteusTok = new ProteusTok();
		Field[] proteusToks = proteusTok.getClass().getDeclaredFields();
		proteusTermNames = new String[proteusToks.length];
		for (int f = 0; f < proteusToks.length; f++) {
			try {
				int tok = proteusToks[f].getInt(proteusTok);
				String lex = proteusToks[f].toString().replaceAll("^.*\\.", "");
				proteusTermNames[tok] = lex;
			}
			catch (IllegalAccessException _) {}
		}
	}

	/** Izvede fazo leksikalne analize (ce ta ni del sintaksne analize). */
	public static void exec() {
		System.out.println("Lexicographical analysis...");
		/* Odpremo vhodno in izhodno datoteko.  */
		FileReader srcFile = null;
		String srcName = compiler.Main.prgName + ".proteus";
		try { srcFile = new FileReader(srcName); }
		catch (FileNotFoundException _) { Report.error("Source file '" + srcName + "' cannot be opened.", 1); }
		PrintStream xml = XML.open("lexanal");

		/* Opravimo leksikalno analizo: zgolj beremo simbol za simbolom.  */
        ProteusLex lexer = new ProteusLex(srcFile);
        ProteusSym symbol;
        try {
            while ((symbol = lexer.next_token ()).sym != ProteusTok.EOF) {
            	symbol.toXML(xml);
            }
        }
        catch (IOException _) {
            Report.error("Error while testing lexical analyzer.", 1);
        }

        /* Zapremo obe datoteki.  */
        XML.close("lexanal", xml);
        try { srcFile.close(); }
		catch (IOException _) { Report.error("Source file '" + srcName + "' cannot be opened.", 1); }
	}

}
