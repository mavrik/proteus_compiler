package compiler.symtable;

/** Ob iskanju deklaracije imena, ki ni deklarirano. */
public class SymUndefinedAtThisScope extends Exception {

	private static final long serialVersionUID = 0L;

	public SymUndefinedAtThisScope(String message) {
		super(message);
	}
}
