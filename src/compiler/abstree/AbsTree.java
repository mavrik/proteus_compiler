package compiler.abstree;

import java.io.*;

import compiler.report.*;
import compiler.semanal.*;

/**
 * Abstraktno sintaksno drevo.
 *
 * (Da se izognemo celi vrsti metod za delo s polji razredov paketa
 * `compiler.abstree', so vsa polja (in vse metode) v paketu `compiler.abstree'
 * definirane kot `public'. Pri programiranju se drzimo pravila, da vrednosti
 * teh polj le berete, nastavljate pa jih le preko konstruktorjev.)
 *
 * @see AbsDecl
 * @see AbsDecls
 * @see AbsTypExpr
 * @see AbsValExpr
 * @see AbsStmt
 * @see AbsStmts
 */
public abstract class AbsTree implements XMLable {

	/**
	 * Vrstica prvega znaka simbola, s katerim se zacne koda, opisana s tem
	 * drevesom.
	 */
	public int begLine;

	/**
	 * Stolpec prvega znaka simbola, s katerim se zacne koda, opisana s tem
	 * drevesom.
	 */
	public int begColumn;

	/**
	 * Vrstica prvega znaka simbola, s katerim se konca koda, opisana s tem
	 * drevesom.
	 */
	public int endLine;

	/**
	 * Stolpec prvega znaka simbola, s katerim se konca koda, opisana s tem
	 * drevesom.
	 */
	public int endColumn;

	public AbsTree() {
		this.begLine = -1;
		this.begColumn = -1;
		this.endLine = -1;
		this.endColumn = -1;
	}

	/** Doloci zacetek te stavcne oblike v vhodni datoteki. */
	public void setBeg(int line, int column) {
		begLine = line;
		begColumn = column;
	}

	/** Doloci zacetek te stavcne oblike v vhodni datoteki. */
	public void setBeg(AbsTree tree) {
		begLine = tree.begLine;
		begColumn = tree.begColumn;
	}

	/** Doloci konec te stavcne oblike v vhodni datoteki. */
	public void setEnd(int line, int column) {
		endLine = line;
		endColumn = column;
	}

	/** Doloci konec te stavcne oblike v vhodni datoteki. */
	public void setEnd(AbsTree tree) {
		endLine = tree.endLine;
		endColumn = tree.endColumn;
	}

	public abstract Object accept(Visitor v);

	public void descsToXML(PrintStream xml) {
		if (semTyp != null) {
			semTyp.toXML(xml);
		}
	}

	// SEMANTICNA ANALIZA:

	public SemTyp semTyp = null;

}
