package compiler.liveness;

import java.util.*;

import compiler.imcode.*;
import compiler.asmcode.*;

public class Main {

	/** Izvede prevajanje do faze izracuna interferen"cnega grafa.  */
	public static void exec() {
		/* Prevajanje do faze izracuna linearizirane vmesne kode.  */
		compiler.lincode.Main.exec();
		System.out.println("Determining variable lifetime...");
		/* Testni izpis generirane kode za vse funkcije.  */
		
		// determing
		Iterator<ImcChunk> chunks = compiler.imcode.Main.chunks.iterator();
		while (chunks.hasNext()) {
			ImcChunk chunk = chunks.next();
			if (chunk instanceof ImcCodeChunk) {
				ImcCodeChunk codeChunk = (ImcCodeChunk)chunk;
				LinkedList<AsmInstr> asmCode = compiler.asmcode.Main.generateAsmCode(codeChunk.lincode, codeChunk.frame);
				System.out.print("\nFUNCTION " + codeChunk.frame.fun.name + ":\n");
				Iterator<AsmInstr> instrs = asmCode.iterator();
				while (instrs.hasNext()) {
					AsmInstr instr = instrs.next();
					System.out.println(instr.format(null));
				}
				System.out.print("\nINTERFERENCE GRAPH:\n");
				LinkedList<LivenessEdge> graph = LivenessAnalyser.analyze(asmCode);
				
				for (LivenessEdge edge : graph)
				{
					System.out.println(edge);
				}
			}
		}
	}
}
