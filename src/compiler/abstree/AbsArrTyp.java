package compiler.abstree;

import java.io.*;

/** Opis tabel. */
public class AbsArrTyp extends AbsTypExpr {

	/** Dimenzija tabele. */
	public AbsValExpr dim;

	/** Tip elementa tabele. */
	public AbsTypExpr base;

	public AbsArrTyp(AbsValExpr dim, AbsTypExpr base) {
		this.dim = dim;
		this.base = base;
	}

	@Override
	public void toXML(PrintStream xml) {
		xml.print("<absnode kind=\"ARR\">\n");
		descsToXML(xml);
		dim.toXML(xml);
		base.toXML(xml);
		xml.print("</absnode>\n");
	}

	@Override
	public Object accept(Visitor v) { return v.visit(this); }

}
